"""
Writing configs to /etc/schroot (which is optional) requires sudo
"""
from datetime import datetime
import logging
import os
from pathlib import Path
import subprocess

from schroot import SchrootConfig

log = logging.getLogger(__name__)

DNF_CONF = Path.home() / "etc/dnf/dnf.conf"


def mmdebstrap(
        distro, path, components=("main", "universe"), packages=(), sources=None,
        mmdebstrap_args=("--mode", "sudo", "--variant", "buildd")
):
    if components:
        mmdebstrap_args = [*mmdebstrap_args, "--components", " ".join(components)]
    if packages:
        mmdebstrap_args = [*mmdebstrap_args, "--include", ",".join(packages)]
    command = ["mmdebstrap", *mmdebstrap_args, distro, str(path)]
    log.debug("Running %r", command)
    return subprocess.run(
        command, input=sources.encode() if sources else None, check=True
    )


def dnf(
        distro,
        path,
        packages=("dnf",),
        dnf_args=("--config", str(DNF_CONF)),
        sources=(),
        lang="en",
):
    """
    Preparation on Debian and derivatives: (see README.Debian)
    1. mkdir /var/lib/rpm
    2. apt install dnf rpm
    3. rpm --initdb
    4. Find packages fedora-gpg-keys-$VERSION.noarch.rpm and fedora-repos-$VERSION.noarch.rpm and:
       cd / ; rpm2cpio $that | cpio -idmv
    5. dnf update --releasever $VERSION
    """
    # "--assumeyes",
    assert distro
    path = Path(path).absolute()
    # Default minimal set of packages
    packages = [
        "rootfiles",
        "system-release",
        "passwd",
        "rtkit",
        "policycoreutils",
        *packages,
    ]
    if lang:
        # Otherwise, all languages are added
        packages = [f"glibc-langpack-{lang.lower()}", *packages]

    dnf_install_args = [f"--releasever={distro}", f"--installroot={path}"]
    if sources:
        for element in sources:
            if isinstance(element, str):
                dnf_install_args.append(f"--repo={element}")
            elif isinstance(element, (tuple, list)):
                label, repo_path = element
                dnf_install_args.extend((f"--repofrompath", f"{label},{repo_path}"))
            else:
                raise ValueError(element)
    command = ["dnf", *dnf_args, "install", *dnf_install_args, *packages]
    log.debug("Running %r", command)
    return subprocess.run(command, check=True)


def debootstrap(distro, path, debootstrap_args=()):
    command = ["debootstrap", *debootstrap_args, distro, str(path)]
    log.debug("Running %r", command)
    return subprocess.run(command, check=True)


def mkchroot(name, distro, path, packages=("ca-certificates", "systemd-container")):
    if os.path.sep not in str(path):
        path = Path("/var/lib/machines") / path
    else:
        path = Path(path)
    if distro.startswith("fedora"):
        dnf(
            distro.replace("fedora", "") or "/",  # man dnf
            path,
            packages=packages,
        )
    elif distro == "buster":
        mmdebstrap(
            distro,
            path,
            packages=packages,
            sources=f"""\
deb [arch=amd64] http://cdn-fastly.deb.debian.org/debian/ {distro} main contrib non-free
deb [arch=amd64] http://cdn-fastly.deb.debian.org/debian/ {distro}-updates main contrib non-free
deb [arch=amd64] http://security.debian.org/ {distro}/updates main contrib non-free
""",
        )
    elif distro == "precise":
        mmdebstrap(
            distro,
            path,
            packages=packages,
            mmdebstrap_args=[
                "--keyring",
                # See package ubuntu-keyrings
                "/usr/share/keyrings/ubuntu-archive-removed-keys.gpg",
            ],
        )
    elif distro == "xenial":
        mmdebstrap(distro, path, packages=packages)
    else:
        debootstrap(distro, path)
    config = SchrootConfig(name, {
        "description": f"{distro} created {datetime.now()}",
        "directory": str(path.absolute()),
    })
    if config.config_root.exists():
        if os.access(config.config_root, os.W_OK):
            config.write_config()
        else:
            log.warning(
                "Cannot write to '%s', skipping schroot config", config.config_root
            )


if __name__ == "__main__":
    import sys

    logging.basicConfig(level=logging.DEBUG)
    _, name, source, *args = sys.argv
    if "://" in source:
        url = source
        command = ["machinectl", "pull-tar" if ".tar" in url else "pull-raw", url, name]
        log.debug("Running %r", command)
        subprocess.run(command, check=True)
    else:
        distro = source
        [path] = args
        mkchroot(name, distro, path)
