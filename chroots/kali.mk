TMPDIR ?= $( mktemp -d )

%.deb :
	mkdir -p "$(@D)"
	curl -fL -o "$@" 'http://http.kali.org/pool/main/k/kali-archive-keyring/$(@F)'
