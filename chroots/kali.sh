#! /bin/sh
export VERSION='2022.1'
set -eu
packaging_prefix=/var/chroot
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
cache_dir="$XDG_CACHE_HOME/apt/archives"

cd "$installer_dir"
  if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
    $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
  fi
  ver_keyring=$( dpkg-query -f '${Version}' -W kali-archive-keyring )
  if dpkg --compare-versions "$ver_keyring" lt "$VERSION"; then
    make -rf "${package_name}.mk" \
      "${cache_dir}/kali-archive-keyring_${VERSION}_all.deb"
    $SUDO dpkg -i "${cache_dir}/kali-archive-keyring_${VERSION}_all.deb"
  fi
  stow -S -t "$HOME" --no-folding "$package_name"
cd -

# Check we have a good debootstrap
ver_debootstrap=$( dpkg-query -f '${Version}' -W debootstrap )
case "$ver_debootstrap" in
  *kali*) ;;
  *) dpkg --compare-versions "$ver_debootstrap" ge "1.0.97" ;;
esac

$SUDO debootstrap "kali-rolling" "${install_prefix}.chroot"
# Notes:
# - add non-free to sources list
