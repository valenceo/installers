#! /bin/sh
export VERSION='XXX'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/XXX
cd build/XXXX
  cmake -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DCMAKE_BUILD_TYPE=Release \
    $source_dir/
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
