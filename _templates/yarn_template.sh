#! /bin/sh -x
export VERSION=XXX
set -eu
install_prefix="${HOME}/.local/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )
export PATH="$PATH:${source_dir}/node_modules/.bin"

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  npm install --force yarn
  yarn install
  yarn dist
  [ -n "$install_prefix" ] && mkdir "$install_prefix" && cp -Ruv dist/linux-unpacked/* "$install_prefix"
cd -
