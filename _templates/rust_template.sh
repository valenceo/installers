#! /bin/sh
set -eu

. /etc/environment.d/50-rust.conf
export PATH

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

cargo install "$package_name"
