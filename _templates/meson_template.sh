#! /bin/sh
export VERSION='XXX'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

meson setup \
  --prefix="$install_prefix" \
  --localstatedir="$XDG_STATE_HOME" \
  --sharedstatedir="$XDG_STATE_HOME" \
  --sysconfdir="$HOME/etc" \
  -Dlocategroup=$( id -gn ) \
  -Dsystemunitdir="$XDG_CONFIG_HOME/systemd/user" \
  build/XXX "$source_dir"
ninja -C build/XXX install

cd "$installer_dir"
  . hooks/post_install.sh
cd -
