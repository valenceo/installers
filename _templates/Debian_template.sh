#! /bin/sh
set -eu

cd "$installer_dir"
  keyring=XXX
  $SUDO make -rf "${package_name}.mk" "$keyring"
  $SUDO tee /etc/apt/sources.list.d/${package_name}.list << EOF
deb [arch=$(dpkg --print-architecture) signed-by=${keyring}] XXX
EOF
  $SUDO apt install apt-transport-https
  $SUDO apt update
  if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
    $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
  fi
cd -
