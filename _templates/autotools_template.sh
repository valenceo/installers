#! /bin/sh -eu
anitya_project_id=XXX
package_name=XXX

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"
mkdir -p ~/build/${package_name}
cd ~/build/${package_name}
  "${source_dir}/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir="${HOME}/etc" \
    --with-ssl-dir="${OPENSSL_ROOT_DIR?}"
  make -j -l $(nproc)
  make install
cd -
. ${hooks_dir}/post_install.sh
