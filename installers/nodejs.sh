#! /bin/sh
export VERSION=16.14.2
set -eu
install_prefix="~/.nodenv/versions/${VERSION}"
# Note the unusual spelling:
source_dir=$( realpath "src/node-v${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

# --with-intl=system-icu: use the system version of icu.
# To use system icu, a fairly new version must be available
# Other values are full-icu (to build a local, full icu library) and small-icu (to build a local, minimal icu library).
cd "$source_dir"
  $source_dir/configure \
    --prefix="$install_prefix" \
    --ninja \
    --shared-libuv \
    --shared-nghttp2 \
    --shared-openssl \
    --shared-zlib \
    --with-intl=small-icu
  make -j -l $(nproc)
  make install
cd -

if ! nodenv rehash; then
  echo "Manually set PATH=${install_path}/bin:\$PATH for your .env"
fi
