#! /bin/sh
VERSION="12.0.0-17287072"
set -e
packaging_prefix="/opt/stow"
install_prefix="${packaging_prefix}/VMware-Remote-Console"

$SUDO sh "${TMPDIR}/VMware-Remote-Console-${VERSION}.x86_64.bundle" \
  --prefix="$install_prefix"
chown -R :staff "$install_prefix"
