#! /bin/sh
# feed at http://blog.calhariz.com/index.php/feed/tag/at/atom
export VERSION='3.2.5'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}" \
    --sysconfdir=$HOME/etc \
    --with-etcdir=$HOME/etc \
    --with-systemdsystemunitdir=${XDG_CONFIG_HOME}/systemd/user \
    --with-daemon_username=$( id -un ) \
    --with-daemon_groupname=$( id -gn ) \
    --with-loadavg_mx=1.5 \
    --with-jobdir="${XDG_STATE_HOME}/cron/atjobs" \
    --with-atspool="${XDG_STATE_HOME}/cron/atspool" \
    PIDDIR="${XDG_RUNTIME_DIR}" \
    SENDMAIL="${HOME}/.local/lib/cron-sendmail"
  make all -j -l $(nproc)
  fakeroot make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
  install -d -m 0700 "${XDG_STATE_HOME}/cron/atjobs" "${XDG_STATE_HOME}/cron/atspool" 
cd -
