#! /usr/bin/make -rf
module_directory = $(lastword $(wildcard ${HOME}/go/pkg/mod/github.com/junegunn/fzf@*/))

%/bin/fzf :
	go install "github.com/junegunn/$(@F)@latest"
	install -s "${HOME}/go/bin/fzf" "$@"

%/bin/fzf-tmux :
	install -C -D "${module_directory}/bin/$(@F)" "$@"

%/fzf/plugin :
	install -d "$(@D)"
	cp -Ruv "${module_directory}/plugin" "$(@D)"

%/fzf.vim/plugin :
	-git clone "https://github.com/junegunn/fzf.vim.git" "$(@D)"

%/bash_completion.d/${package_name}.bashrc %/bash-completion/completions/${package_name}.bashrc :
	install -C -D "${module_directory}/shell/completion.bash" "$@"

%/bash/${package_name}-key-bindings.bashrc : 
	install -C -D "${module_directory}/shell/key-bindings.bash" "$@"
