#! /bin/sh
project_id=5405
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

codename=$( lsb_release -sc )

if dependencies=$( sed 's/[#].*//' "${package_name}-${codename}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

type nasm || install_package nasm

mkdir -p build/ffmpeg
cd build/ffmpeg
  $source_dir/configure \
    --prefix="$install_prefix" \
    --disable-static --as=yasm --enable-gpl \
    --enable-nonfree --enable-shared \
    --enable-chromaprint --enable-fontconfig \
    --enable-libcaca --enable-libfdk-aac \
    --enable-libfreetype --enable-libfontconfig --enable-libmp3lame --enable-libopus \
    --enable-libtheora --enable-libvorbis --enable-libvpx \
    --enable-libwebp --enable-libx264 --enable-libx265 \
    --enable-openssl 
  make -j -l $(nproc)
  make install
cd -
. "${hooks_dir}/post_install.sh"
