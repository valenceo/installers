#! /bin/sh
export VERSION='2.12'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/pqiv
cd build/pqiv
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --sysconfdir=$HOME/etc
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
