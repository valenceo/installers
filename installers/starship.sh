#! /bin/sh
. templates/rust_template.sh

install -D -s ~/.cargo/bin/${package_name} ~/bin/${package_name}
install -d "${XDG_CONFIG_HOME}/bash"
<< EOF cat > ${XDG_CONFIG_HOME}/bash/${package_name}.bashrc
eval "\$( ~/bin/${package_name} init bash )"
EOF
