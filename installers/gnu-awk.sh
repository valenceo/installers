#! /bin/sh
export VERSION='5.1.1'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/gawk-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/gawk
cd build/gawk
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --sysconfdir=$HOME/etc \
    --disable-nls
  
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
