#! /usr/bin/env make -rf
include ${installer_dir}/Makefile

install : ${HOME}/.vim/pack/plugins/start/${package_name} ${HOME}/.vim/pack/plugins/start/${package_name}/doc ${HOME}/.vim/${package_name}.vimrc ${HOME}/bin/vimwiki

%/.vim/${package_name}.vimrc :
	install "${HOME}/examples/${package_name}.vimrc" "$@"

%/${package_name} :
	git clone --depth=1 https://github.com/vimwiki/${package_name}.git "$@"

%/vimwiki/doc :
	nvim -c "helptags $@" -c quit

# Note: Python package called vimwiki-cli with source under vimwiki_cli
%/bin/vimwiki :
	${SHIV} -o "$@" -c vimwiki vimwiki-cli

# Broken:
%/bash_completion.d/vimwiki-cli.bashrc %/bash-completion/completions/vimwiki-cli.bashrc :
	install -d "$(@D)"
	env _VIMWIKI_COMPLETE=source_bash vimwiki > "$@"
