%/src/${project_name} :
	git clone -b "v${VERSION}" --depth=1 'https://github.com/malaterre/GDCM' "$@"
