
let g:ale_lint_on_enter = 0
let g:ale_fixers = { 'python': ['black'] }

" Make sure you use single quotes in paths
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

