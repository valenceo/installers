#! /usr/bin/make -rf
arch ?= linux-amd64
#arch = linux-386

install_prefix := ${packaging_prefix}/${package_name}-${VERSION}
TMPDIR ?= $(shell mktemp -d)

install : ~/build/${package_name}-${VERSION}
	install -s "$</git-lfs" -D "${install_prefix}/bin/git-lfs"
	install -d "${install_prefix}/share/"
	cp -Ruv "$</man" "${install_prefix}/share/"

%/build/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${arch}-v${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/${package_name}-% :
	curl -fL -o "$@" "https://github.com/git-lfs/${package_name}/releases/download/v${VERSION}/$(@F)"

.PHONY: install
