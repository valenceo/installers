#! /bin/sh
set -eu
make -rf "${package_name}.mk" \
  /etc/debsig/policies/AC2D62742012EA22/1password.pol \
  /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg \
  /usr/share/keyrings/1password-archive-keyring.gpg
install -m 0644 "${package_name}.list" /etc/apt/sources.list.d/teams.list

apt update
apt install 1password 1password-cli

make -rf "${package_name}.mk" \
  /etc/bash_completion.d/${package_name}.bashrc
