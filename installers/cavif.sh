#! /bin/sh
set -eu

if ! type nasm; then
  install-package nasm
fi
if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

cargo install "$package_name"
install -s ~/.cargo/bin/${package_name} ~/bin
