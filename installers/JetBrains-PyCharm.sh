#! /bin/sh
project_id=227263
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
export TMPDIR=$( mktemp -d )

install_prefix="$HOME/.local/pycharm-$VERSION"
#install_prefix="$HOME/.local/pycharm-community-$VERSION"

command -v shellcheck || install-package shellcheck || :

if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$install_prefix"
fi

# Maybe StartupWMClass needs to be tweaked for Professional
<< EOF cat > "${TMPDIR}/${package_name}.desktop"
[Desktop Entry]
Type=Application
Version=1.0
Name=PyCharm ${VERSION}
StartupWMClass=jetbrains-pycharm-ce
Exec="${install_prefix}/bin/pycharm.sh" %u
Terminal=false
Categories=Development;IDE;Java;
EOF

desktop-file-install --dir="${XDG_DATA_HOME}/applications" \
  --delete-original --rebuild-mime-info-cache --copy-name-to-generic-name \
  --set-icon="${install_prefix}/bin/pycharm.svg" \
  "${TMPDIR}/${package_name}.desktop"

# Maybe only in Professional offering...
if [ -d "$install_prefix/debug-eggs" ]; then
  install -d ~/python/
  install -C -D "$install_prefix/debug-eggs"/* ~/python/
fi
