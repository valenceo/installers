#! /usr/bin/env make -rf
VERSION ?= 2.15.05
TMPDIR ?= $(shell mktemp -d)

%/src/nasm-${VERSION} : ${TMPDIR}/nasm-${VERSION}.tar.bz2
	tar -C "$(@D)" -xaf "$<"
	cd "$@" ; sh autogen.sh

${TMPDIR}/nasm-% :
	curl -fL -o "$@" "https://www.nasm.us/pub/nasm/releasebuilds/${VERSION}/$(@F)"
