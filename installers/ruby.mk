#! /usr/bin/env make -rf

TMPDIR ?= $(shell mktemp -d)

%/src/ruby-${VERSION} : ${TMPDIR}/ruby-${VERSION}.tar.xz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/ruby-% :
	curl -fL -o "$@" "https://cache.ruby-lang.org/pub/ruby/2.7/$(@F)"
