#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name} :
	git clone -b v${VERSION} https://github.com/caddyserver/${package_name}.git "$@"
