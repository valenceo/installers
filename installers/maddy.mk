#! /usr/bin/env make -rf

%/src/${package_name} :
	git clone --depth=1 -b v${VERSION} "https://github.com/foxcpp/$(@F).git" "$@"

