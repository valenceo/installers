#! /usr/bin/make -rf
VERSION ?= 0.11.1
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name} :
	mkdir -p "$(@D)"
	git clone --depth=1 -b "v${VERSION}" "https://github.com/tstack/${package_name}" "$@"
	cd "$@" ; sh autogen.sh
