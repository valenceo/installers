#! /usr/bin/make -rf
install : ${XDG_CONFIG_HOME}/bash/${package_name}.bashrc

%/bash/${package_name}.bashrc :
	curl -fL -o "$@" "https://raw.githubusercontent.com/chubin/wttr.in/master/share/bash-function.txt"
