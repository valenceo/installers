#! /usr/bin/make -rf

TMPDIR ?= $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/rsync-% :
	curl -fL -o "$@"     "https://www.samba.org/ftp/rsync/src/$(@F)" \
	         -o "$@.asc" "https://www.samba.org/ftp/rsync/src/$(@F).asc"
	-gpg --verify "$@.asc"
