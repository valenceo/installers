#! /usr/bin/make -rf

%/src/${package_name}-${VERSION} :
	git clone --depth=1 -b "v${VERSION}" \
		'https://code.blicky.net/yorhel/${package_name}.git' "$@"
	cd "$@" ; autoreconf -fi
