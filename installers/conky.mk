#! /usr/bin/env make -rf

%/src/${package_name} :
	git clone --depth=1 -b "v${VERSION}" "git@github.com:brndnmtthws/${package_name}.git" "$@"

%/conky/conky.conf :
	mkdir -p "$(@D)"
	conky --print-config > "$@"
