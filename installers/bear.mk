#! /usr/bin/env make -rf

%/src/${package_name} :
	git clone --depth=1 "https://github.com/rizsotto/Bear.git" "$@"
