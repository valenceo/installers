#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/less-% :
	curl -fL -o "$@"     "https://www.greenwoodsoftware.com/less/less-${VERSION}.tar.gz" \
	         -o "$@.sig" "https://www.greenwoodsoftware.com/less/less-${VERSION}.sig"
	-gpg --verify "$@.sig"
