#! /usr/bin/make -rf

%/src/${package_name} :
	git clone --depth=1 -b ${VERSION} https://github.com/tmux/tmux.git "$@"
	cd "$@" ; sh autogen.sh

${HOME}/.tmux.conf :
	install -m0644 $(@D)/examples/tmux.conf $@

%/tmux/plugins/tpm :
	install -d "$(@D)"
	git clone --depth=1 git@github.com:tmux-plugins/tpm $@

%/bash_completion.d/${package_name}.bashrc %/bash-completion/completions/${package_name}.bashrc :
	curl -fL --create-dirs -o "$@" "https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux"

tmux-245color.info :
	[ -s /usr/share/t/terminfo/tmux-256color.info ] || curl -fl -o tmux-256color.info 'https://gist.githubusercontent.com/nicm/ea9cf3c93f22e0246ec858122d9abea1/raw/37ae29fc86e88b48dbc8a674478ad3e7a009f357/tmux-256color'
	# Suitable for tic -xe tmux-256color tmux-256color.info
