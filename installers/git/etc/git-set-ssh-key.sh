#! /bin/sh
set -e
if command=$( git config core.sshCommand ); then
  echo "Command already set: $command"
elif [ $# -gt 0 ]; then
  identity="$1"
  command shift
  [ -s "$identity" ] || ssh-keygen -t ed25519 -f "$identity"
  echo "Set with 'git config core.sshCommand \"ssh -i $identity -F /dev/null\"'"
else
  echo "Usage:" >&2
  echo "  git set-ssh-key name-of-key-here" >&2
  echo "If the key doesn't exist, it will be created" >&2
  exit 10
fi
