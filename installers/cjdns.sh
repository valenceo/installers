#! /bin/sh
export VERSION=21.2
set -eu
source_dir=$( realpath "src/${package_name}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi

cd "$source_dir"
  ./clean
  ./do
  $SUDO install -D -s ./cjdroute /usr/bin/cjdroute
cd -

cd "$installer_dir"
  $SUDO make -rf "${package_name}.mk" package_name="$package_name" \
    /etc/cjdroute.conf /etc/systemd/system/cjdns.service /etc/systemd/system/cjdns-resume.service
  $SUDO systemctl daemon-reload
cd -

