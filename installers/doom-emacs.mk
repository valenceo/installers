#! /usr/bin/env make -rf

${HOME}/.emacs.d :
	install -d "${XDG_CONFIG_HOME}/doom"
	git clone --depth=1 "https://github.com/hlissner/doom-emacs" "$@"
	"$@/bin/doom" install
