install : src/quickemu
	sed 's/[\#].*//' "${package_name}.deps" | xargs -r -d '\n' sudo apt install --no-install-recommends --no-install-suggests

src/quickemu/macrecovery.py src/quickemu/boards.json src/quickemu/recovery_urls.txt :
	curl -o "$@" -fL "https://raw.githubusercontent.com/acidanthera/OpenCorePkg/master/Utilities/macrecovery/$(@F)"

src/quickemu :
	git clone --depth=1 "https://github.com/wimpysworld/$(@F).git" "$@"
