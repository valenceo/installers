#! /usr/bin/env make -rf

%/bash_completion.d/${package_name} :
	install -d "$(@D)"
	gh completion -s bash > $@

%/githubcli-archive-keyring.gpg :
	curl -fL "https://cli.github.com/packages/$(@F)" > "$@"

