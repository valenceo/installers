#! /usr/bin/env make -rf

%/share/oix-route-views/oix-full-snapshot-latest.dat.bz2 :
	curl -fL --create-dirs -o "$@" "http://archive.routeviews.org/oix-route-views/$(@F)"
