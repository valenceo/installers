#! /usr/bin/make -rf
install_prefix = ${HOME}
VERSION = 2022.10.0

DOWNLOAD_DIR := $(shell xdg-user-dir DOWNLOAD)
TMPDIR ?= $(shell mktemp -d)

install : ${install_prefix}/bin/bw

%/bin/bw : ${DOWNLOAD_DIR}/bw-linux-${VERSION}.zip
	mkdir -p "$(@D)"
	unzip -d "$(@D)" "$<"
	chmod +x "$@"
