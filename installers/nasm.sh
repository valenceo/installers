#! /bin/sh -eu
VERSION=2.15.05  # Maybe needed by ffmpeg?
anitya_project_id=2048
package_name=nasm

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"
mkdir -p ~/build/${package_name}
cd ~/build/${package_name}
  ${source_dir}/configure \
    --prefix="$install_prefix"
  make -j -l $(nproc)
  make install
cd -

