%/src/hashdeep :
	git clone --depth=1 -b "v${VERSION}" "https://github.com/jessek/hashdeep" "$@"
	cd "$@" && autoreconf -fi
