#! /bin/sh
export VERSION='4.4'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/hashdeep
cd build/hashdeep
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=$HOME/etc
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
