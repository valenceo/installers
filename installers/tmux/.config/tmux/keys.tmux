
# see neovim :checkhealth
set-option -sq escape-time 10

bind c new-window -ac "#{pane_current_path}"
bind C-l refresh-client
bind R source-file ~/.tmux.conf \; display-message "Config reloaded..."

# split panes using | and -
unbind '"'
unbind %
bind | split-window -h
bind - split-window -v

bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel

### Deprecated:
#set -g status-utf8 on

set-window-option -g mode-keys vi
set -g mouse on
unbind m
bind m set -g mouse

bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# urxvt tabs take over S-left, S-right, and S-down
bind -n C-left prev
bind -n C-right next

# handled by tmux-sensible
#bind R source-file ~/.tmux.conf \; display-message "Config reloaded"

# prefix + / to search
bind-key / copy-mode \; send-key ?

