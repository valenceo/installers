# https://github.com/tmux-plugins/tpm
# <prefix>I re-installs plugins

# https://github.com/christoomey/vim-tmux-navigator
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'jbnicolai/tmux-fpp'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @open-S 'https://startpage.com/do/search?q='
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tpm'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.config/tmux/plugins/tpm/tpm'
