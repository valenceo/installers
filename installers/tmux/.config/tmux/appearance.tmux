set -g status-style "fg=white"

# Deprecated in v1.9
#set-window-option -g window-status-current-fg colour179
#set-window-option -g window-status-current-bg colour53
#set-window-option -g window-status-current-attr underscore
set-window-option -g window-status-current-style "fg=colour179,bg=colour53,underscore"

#set-window-option -g window-status-last-fg colour248
#set-window-option -g window-status-last-bg colour238
#set-window-option -g window-status-last-attr underscore
set-window-option -g window-status-last-style "fg=colour248,bg=colour238,underscore"

#set-window-option -g window-status-fg colour248
#set-window-option -g window-status-bg colour238
#set-window-option -g window-status-attr dim
set-window-option -g window-status-style "fg=colour248,bg=colour238,dim"

#set-window-option -g pane-active-border-fg colour179
set-window-option -g pane-active-border-style "fg=colour179"
#set-window-option -g pane-border-fg colour53
set-window-option -g pane-border-style "fg=colour53"

set-option -g clock-mode-colour white

set-option -g set-titles on
set-option -g set-titles-string "tmux: #H"
set-option -g status-position top

set -g status-interval 12
set -g status-left '#{?pane_at_left,,   }#{?client_prefix,#[bg=colour245 fg=colour233 bold],#[fg=colour155]}(#S) '
set -g status-right '"#{=21:pane_title}" %H:%M #{?window_zoomed_flag,🔍,#{?pane_at_right,,   }}'

setw -g xterm-keys on
