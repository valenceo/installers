#! /bin/sh -eu
VERSION=608
anitya_project_id=1550
package_name=less
. hooks/setup.sh
. ${hooks_dir}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"
mkdir -p build/less
cd build/less
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --sysconfdir=/etc \
    --with-editor=sensible-editor
  make all -j -l $(nproc)
  make install
cd -

. ${hooks_dir}/post_install.sh


#LESSCHARSET='UTF-8'
<< EOF cat > "${XDG_CONFIG_HOME}/environment.d/80-${package_name}.conf"
LESS="-eiqwJMRS --tabs=4"
LESS_TERMCAP_mb=$'\e[1;33m'
LESS_TERMCAP_md=$'\e[1;35m'
LESS_TERMCAP_me=$'\e[0m'
LESS_TERMCAP_se=$'\e[0m'
LESS_TERMCAP_so=$'\e[01;44;33m'
LESS_TERMCAP_ue=$'\e[0m'
LESS_TERMCAP_us=$'\e[1;4;31m'

PAGER=less
EOF
