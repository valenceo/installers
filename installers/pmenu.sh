#! /bin/sh
export VERSION='2.5.0'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p "$source_dir"
cd "$source_dir"
  make PREFIX="$install_prefix" MANPREFIX="${install_prefix}/share/man" -j -l $(nproc) install
cd -

cd "$installer_dir"
  make -rf "${package_name}.mk" "${XDG_DATA_HOME}/openmoji"
  . hooks/post_install.sh
cd -
