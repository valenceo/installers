#! /bin/sh
export arch=x86_64
#export arch=i686

project_id=7635
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
build_dir=$( realpath build/rust-${VERSION}-${arch}-unknown-linux-gnu )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$build_dir"
  ./install.sh --disable-ldconfig --prefix="$install_prefix"
cd -

tee /etc/environment.d/50-${package_name}.conf << EOF
PATH="\$PATH:${install_prefix}/bin"
EOF
