#! /bin/sh
export VERSION=1.9.7
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/Orthanc-${VERSION}" )

#VERSION=1.9.7 install-package dcmtk
#VERSION=5.3.6 install-package lua
if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

mkdir -p build/orthanc
cd build/orthanc
  cmake -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DALLOW_DOWNLOADS=ON \
    -DUSE_GOOGLE_TEST_DEBIAN_PACKAGE=ON \
    -DUSE_SYSTEM_CIVETWEB=OFF \
    -DDCMTK_DICTIONARY_DIR=/opt/share/dcmtk \
    -DDCMTK_INCLUDE_DIR=/opt/include \
    -DCMAKE_BUILD_TYPE=Release \
    $source_dir/OrthancServer/
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
