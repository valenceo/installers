#! /bin/sh
project_id=4980
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/tmux
cd build/tmux
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=/etc
  make -j -l $(nproc)
  make install
cd -

cd "${installer_dir}"
  stow -S -t "$HOME" --no-folding "$package_name"
  make -rf "${package_name}.mk" \
    "${XDG_CONFIG_HOME}/tmux/plugins/tpm" \
    "${HOME}/.tmux.conf" \
    "${install_prefix}/etc/bash_completion.d/${package_name}.bashrc"
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -

command_path="$( realpath -e ${packaging_prefix}/../bin/${package_name} )"
if command -v update-shells && ! fgrep -q "$command_path" /var/lib/shells.state; then
  << EOF $SUDO tee -a /var/lib/shells.state
$command_path
EOF
  $SUDO update-shells
fi
