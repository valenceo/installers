#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.bz2
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/${package_name}-% :
	curl -fL -o "$@" "https://nmap.org/dist/$(@F)"
