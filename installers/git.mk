#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/git-${VERSION} : ${TMPDIR}/git-${VERSION}.tar.xz
	mkdir -p $(@D)
	tar -C $(@D) -axf $<
	cd "$@" && autoreconf -fi

${TMPDIR}/git-%.gz ${TMPDIR}/git-%.xz ${TMPDIR}/git-%.sign :
	curl -fL -o $@ https://mirrors.edge.kernel.org/pub/software/scm/git/$(@F)

%/git/config :
	-install -D -m0644 ${HOME}/examples/git-config $@
