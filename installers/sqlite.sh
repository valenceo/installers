#! /bin/sh
export VERSION=3400100
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-autoconf-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/sqlite
cd build/sqlite
  CFLAGS="-DSQLITE_ENABLE_COLUMN_METADATA=1" \
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=$HOME/etc
  make -j -l $(nproc)
  make install
cd -

<< EOF cat > ${XDG_CONFIG_HOME}/environment.d/sqlite.conf
PATH="${install_prefix}/bin:\$PATH"
EOF
