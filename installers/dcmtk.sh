#! /bin/sh
export VERSION=3.6.6
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )
fmjpeg2koj_source_dir=$( realpath "src/fmjpeg2koj" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir" "$fmjpeg2koj_source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

mkdir -p build/dcmtk
cd build/dcmtk
  # OPENSSL_ROOT_DIR for any version up to 3.0.0
  cmake "$source_dir" \
    -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DBUILD_SHARED_LIBS="ON" \
    -DDCMTK_ENABLE_CXX11="ON" \
    -DDCMTK_ENABLE_STL="ON"
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -

# JPEG2000 plugin
mkdir -p build/fmjpeg2koj
cd build/fmjpeg2koj
  cmake "$fmjpeg2koj_source_dir" \
    -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DCMAKE_BUILD_TYPE=RELEASE \
    -DBUILD_SHARED_LIBS="ON" \
    -DBUILD_STATIC_LIBS="OFF" \
    -DDCMTK_ROOT="$install_prefix" \
    -Dfmjpeg2k_ROOT="$fmjpeg2koj_source_dir" \
    -DOpenJPEG_ROOT="/usr"
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -

install -d "$XDG_CONFIG_HOME/environment.d"
cat > $XDG_CONFIG_HOME/environment.d/50-dcmtk.conf << EOF
DCMDICTPATH=${install_prefix}/share/dcmtk/dicom.dic
EOF
