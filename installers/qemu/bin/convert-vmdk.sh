#! /bin/sh
# Following this,
#   kpartx -l image
#   kpartx -a image
#   mount the loopback device as a filesystem
#   unmount and kpartx -d image
ionice qemu-img convert \
  -f vmdk -O raw \
  "$@"
