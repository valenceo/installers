#! /bin/sh
export VERSION='3.25.0'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )
build_dir=$( realpath "build/${package_name}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

mkdir -p build/cmake
cd build/cmake
  # If a recent version of cmake pre-exists
  #cmake "$source_dir" -DCMAKE_INSTALL_PREFIX="$install_prefix"
  #make -j -l $(nproc)
  
  # Otherwise,
  "${source_dir}/bootstrap" --prefix="$install_prefix"
  make -j -l $(nproc)

  make install
cd -

install -d $XDG_CONFIG_HOME/bash
cp -Rus $install_prefix/share/bash-completion/completions/* $XDG_CONFIG_HOME/bash/
