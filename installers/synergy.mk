#! /usr/bin/make -rf
# No Wayland support, yet!

VERSION := 1.14.6-snapshot.88fdd263
DOWNLOAD_DIR := $(shell xdg-user-dir DOWNLOAD)

install : ${DOWNLOAD_DIR}/synergy_${VERSION}_ubuntu22_amd64.deb
	-${SUDO} dpkg -i "$<"
	${SUDO} apt install -f
