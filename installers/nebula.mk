#! /usr/bin/make -rf
arch ?= linux-amd64
#arch = linux-386
TMPDIR ?= $(shell mktemp -d)

%/build/${package_name}-${VERSION} : ${TMPDIR}/nebula-${arch}.tar.gz
	mkdir -p "$@"
	tar -C "$@" -xaf "$<"

${TMPDIR}/nebula-% :
	curl -fL -o "$@" "https://github.com/slackhq/${package_name}/releases/download/v${VERSION}/$(@F)"
