#! /bin/sh
make -rf "${package_name}.mk" ${HOME}/share/oix-route-views/oix-full-snapshot-latest.dat.bz2
stow -S -t "$HOME" --no-folding "${package_name}"
