#! /bin/sh

cd "$installer_dir"
  make -rf "${package_name}.mk" \
    ${XDG_CONFIG_HOME}/bin/cht.sh \
    ${HOME}/.local/etc/bash_completion.d/cht.sh
cd -
