#! /bin/sh
export VERSION='3.7'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/grep
cd build/grep
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=$HOME/etc \
    --disable-nls

  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
