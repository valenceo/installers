#! /bin/sh
cd "$installer_dir"
  . hooks/pre_install.sh
cd -

codename="$(lsb_release -sc)"
make -rf "${package_name}.mk" \
  /usr/share/keyrings/${package_name}-2018.gpg \
  /usr/share/keyrings/${package_name}-2022.gpg
<< EOF cat > /etc/apt/sources.list.d/xpra.list
deb [arch=amd64,arm64 signed-by=/usr/share/keyrings/xpra-2022.gpg] https://xpra.org/ ${codename} main
EOF

apt-get update
apt-get install xpra

cd "$installer_dir"
  . hooks/post_install.sh
cd -

echo "You may need to create a .pth in:"
/usr/bin/python3 -m site
