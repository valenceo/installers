#! /usr/bin/make -rf
arch ?= linux-amd64
#arch = linux-386
TMPDIR ?= $(shell mktemp -d)

%/go : ${TMPDIR}/go${VERSION}.${arch}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf $<

${TMPDIR}/go% :
	curl -fL -o "$@" "https://golang.org/dl/$(@F)"
