#! /bin/sh
set -eu

case $( lsb_release -s -r ) in
  20.10|21.04) $SUDO apt install emacs ;;
  *)
    $SUDO add-apt-repository ppa:kelleyk/emacs
    $SUDO apt update
    $SUDO apt install emacs26
    ;;
esac

cd "$installer_dir"
  make -rf "${package_name}.mk" $HOME/.emacs
cd -
