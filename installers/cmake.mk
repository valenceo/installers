#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/cmake-${VERSION} : ${TMPDIR}/cmake-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/cmake-%.tar.gz ${TMPDIR}/cmake-%-SHA-256.txt ${TMPDIR}/cmake-%-SHA-256.txt.asc :
	curl -fL -o "$@" "https://github.com/Kitware/CMake/releases/download/v${VERSION}/$(@F)"


