#! /bin/sh
export VERSION='26'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  make all -j -l $(nproc)
  make PREFIX="$install_prefix" install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
