#! /bin/sh
export VERSION=6.2.0
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

if [ -z "$( apt-mark showhold 'qemu.*' )" ]; then
  apt-mark hold qemu qemu-kvm qemu-user qemu-user-static
fi

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  git submodule init
  git submodule update --recursive
cd -

mkdir -p build/qemu
cd build/qemu
  "${source_dir}/configure" \
    --prefix="$install_prefix" \
    --localstatedir="${XDG_STATE_HOME}/${package_name}" \
    --sysconfdir=/usr/local/etc \
    --target-list=x86_64-softmmu \
    --disable-sdl \
    --enable-plugins \
    --enable-curl \
    --enable-curses \
    --enable-kvm \
    --enable-spice \
    --enable-spice-protocol \
    --enable-libusb \
    --enable-zstd
  make -j -l $(nproc)
  $SUDO install -g staff -m 0775 -d /usr/local/etc/qemu
  make install
cd -
