#! /bin/sh
export VERSION=2.6.1
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

command -v go || install-package golang
. ${XDG_CONFIG_HOME}/environment.d/50-golang.conf

. "${hooks_dir}/pre_install.sh"

cd "$source_dir"
  cd "${source_dir}/cmd/caddy"
  go build
  install -s caddy -D "${install_prefix}/sbin/${package_name}"
  $SUDO setcap cap_net_bind_service=+ep "${install_prefix}/sbin/${package_name}"
cd -

"${install_prefix}/sbin/caddy" add-package github.com/mholt/caddy-webdav

cd "$installer_dir"
  stow -t "$HOME" --no-folding -S "$package_name"
cd -
. "${hooks_dir}/post_install.sh"
