#! /usr/bin/make -rf

arch ?= amd64
VERSION ?= 5.12.9.367
#arch ?= i386
#VERSION ?= 5.4.53391.1108

TMPDIR ?= $(shell mktemp -d)

install : ${TMPDIR}/zoom_${arch}.deb
	${SUDO} apt install "$<"

${TMPDIR}/zoom_%.deb ${TMPDIR}/zoom_%.rpm :
	curl -fL -o "$@" "https://zoom.us/client/${VERSION}/$(@F)"

