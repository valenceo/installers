#! /bin/sh
export VERSION=1.19.4
set -eu
install_prefix="${packaging_prefix}/go"

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$install_prefix"
fi

tee /etc/environment.d/50-${package_name}.conf << EOF
PATH="\$PATH:${install_prefix}/bin"
EOF
