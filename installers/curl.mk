#! /usr/bin/make -rf

TMPDIR ?= $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/curl-% :
	curl -fL -o "$@"     "https://curl.se/download/$(@F)" \
	         -o "$@.asc" "https://curl.se/download/$(@F).asc"
	-gpg --verify "$@.asc"
