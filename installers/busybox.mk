#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.bz2
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/busybox-%:
	curl -fL -o "$@"        "https://busybox.net/downloads/$(@F)" \
	         -o "$@.sig"    "https://busybox.net/downloads/$(@F).sig" \
	         -o "$@.sha256" "https://busybox.net/downloads/$(@F).sha256"
	-gpg --verify "$@.sig" "$@.sha256"
	cd "$(@D)" ; sha256sum -c "$@.sha256"
