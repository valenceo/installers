#! /bin/sh
export VERSION='1.20.3'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  make HAVE_X11=yes HAVE_GLUT=yes prefix="$install_prefix" install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
