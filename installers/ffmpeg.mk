#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name} :
	git clone --depth 1 -b n${VERSION} "https://git.ffmpeg.org/$(@F).git" "$@"
