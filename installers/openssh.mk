#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/openssh-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/openssh-% :
	curl -fL -o "$@" "https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$(@F)" \
		 -o "$@.asc" "https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$(@F).asc" \
		 -o "$(@D)/RELEASE_KEY.asc" "https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/RELEASE_KEY.asc"
	-gpg --verify "$@.asc"
