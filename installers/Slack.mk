#! /usr/bin/make -rf
VERSION ?= 4.28.184

TMPDIR ?= $(shell mktemp -d)

install : ${TMPDIR}/slack-desktop-${VERSION}-amd64.deb
	apt install "$<"

${TMPDIR}/slack-desktop-%.deb :
	curl -fL -o "$@" "https://downloads.slack-edge.com/releases/linux/${VERSION}/prod/x64/$(@F)"

