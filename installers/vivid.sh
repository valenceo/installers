#! /bin/sh
set -eu
. templates/rust_template.sh

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

cargo install "$package_name"
install -D -s $HOME/.cargo/bin/vivid $HOME/bin
<< EOF cat > ${XDG_CONFIG_HOME}/environment.d/${package_name}.conf
LS_COLORS="$( vivid generate snazzy )"
EOF
