TMPDIR ?= $(shell mktemp -d)

%/src/node-v${VERSION} : ${TMPDIR}/node-v${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/node-% : ${TMPDIR}/SHASUMS256.txt
	curl -fL -o "$@" "https://nodejs.org/dist/v${VERSION}/$(@F)"
	cd "$(@D)" && sha256sum -c --ignore-missing "$(<F)"

# Note the unusual spellings:
${TMPDIR}/SHASUMS256.txt :
	curl -fL \
		-o "$@" "https://nodejs.org/dist/v${VERSION}/$(@F)" \
		-o "$@.sig" "https://nodejs.org/dist/v${VERSION}/$(@F).sig"
	gpg --verify "$@.sig" "$@"
