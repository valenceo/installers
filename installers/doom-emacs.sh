#! /bin/sh
cd "$installer_dir"
  install-package emacs
  make -rf doom-emacs.mk
  stow -t "$HOME" --no-folding -S doom-emacs
cd -

