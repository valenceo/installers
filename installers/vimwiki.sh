#! /bin/sh
cd "$installer_dir"
  stow -S -t "$HOME" --no-folding "$package_name"
  make -rf "${package_name}.mk" install
cd -
