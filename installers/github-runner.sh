#! /bin/sh
install_prefix=/var/github-runner
getent passwd github-runner || useradd -r \
  -g nogroup -d "$install_prefix" -s /bin/false \
  github-runner

make -rf "${package_name}.mk" \
  "${install_prefix}/.dotnet" \
  "${install_prefix}/actions-runner"
