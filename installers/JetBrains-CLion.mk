DOWNLOAD_DIR ?= ${shell xdg-user-dir DOWNLOAD}

%.tar.gz :
	@echo "Download '$@' from 'https://www.jetbrains.com/clion/download/'"

%/clion-${VERSION} : ${DOWNLOAD_DIR}/CLion-${VERSION}.tar.gz
	cd "$(<D)" && sha256sum -c "$<.sha256"
	tar -C "$(@D)" -xaf "$<"
