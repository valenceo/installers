#! /bin/sh
set -eu

cd "$installer_dir"
  keyring=/etc/apt/trusted.gpg.d/githubcli-archive-keyring.gpg
  $SUDO make -rf "${package_name}.mk" "$keyring"
  $SUDO tee /etc/apt/sources.list.d/${package_name}.list << EOF
deb [arch=$(dpkg --print-architecture) signed-by=${keyring}] https://cli.github.com/packages stable main
EOF
  $SUDO apt install apt-transport-https
  $SUDO apt update
  if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
    $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
  fi
  make -rf "${package_name}.mk" \
    "$HOME/.local/etc/bash_completion.d/${package_name}"
cd -
