# source this
case $packaging_prefix in
  */stow|/opt/${SCHROOT_CHROOT_NAME})
    cd "$packaging_prefix"
      stow -S -t "$HOME" --no-folding "${package_name}-${VERSION}"
    cd -
    ;;
esac
