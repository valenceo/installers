env_path=$( realpath ".build-env" )
script_path=$( realpath "$0" )

if [ -z "$VERSION" ] && [ -n "$anitya_project_id" ]; then
  check_url="https://release-monitoring.org/api/v2/versions/?project_id=${anitya_project_id}"
  VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )
fi

# Defaults:
if ! [ -x "$env_path" ]; then
  # OPENSSL_ROOT_DIR points to the latest installed openssl. *This is not supported by all buildable packages*
  if [ -z "$OPENSSL_ROOT_DIR" ] && [ -d /opt/openssl-3.0.8 ]; then
    OPENSSL_ROOT_DIR=/opt/openssl-3.0.8
  else
    OPENSSL_ROOT_DIR=/usr
  fi
  
  # Decide whether we install system-wide or personally
  if [ -n "$packaging_prefix" ]; then
    mkdir -p "$packaging_prefix"
  elif [ -w "/opt" ]; then
    umask 0002
    packaging_prefix="/opt/stow"
  elif [ -w "/usr/local" ]; then
    umask 0002
    packaging_prefix="/usr/local/stow"
  elif [ -w "$HOME/.local/stow" ]; then
    packaging_prefix="$HOME/.local/stow"
  else
    echo "You should probably create a place to store packages"
    echo "sudo install -g staff -m 0775 -d /opt /opt/stow"
    exit 10
  fi >&2
  
  << EOF cat > .build-env
# Customized from $script_path
export OPENSSL_ROOT_DIR="$OPENSSL_ROOT_DIR"

export XDG_CACHE_HOME="$XDG_CACHE_HOME"
export XDG_CONFIG_HOME="$XDG_CONFIG_HOME"
export XDG_DATA_HOME="$XDG_DATA_HOME"
export XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR"
export XDG_STATE_HOME="$XDG_STATE_HOME"

export hooks_dir="$( realpath _hooks )"
export packaging_prefix="$packaging_prefix"
EOF
fi
. "$env_path"
export VERSION

if [ -t 0 ]; then
  SUDO=sudo
else
  SUDO="sudo -n"
fi

codename=$( lsb_release -sc )
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
mkdir -p "${HOME}/src"
source_dir=$( realpath -m "${HOME}/src/${package_name}-${VERSION}" )
