# source this
if key_ids=$( sed 's/[#].*//' "${package_name}.key-ids" ); then
  for arg in $key_ids; do
    case "$arg" in
      https://*) curl -fL "$arg" | gpg --verbose --options "dotfiles-dirmngr.conf" \
        --import ;;
      *) gpg --verbose --options "dotfiles-dirmngr.conf" \
        --receive-keys "$@" ;;
    esac
  done
fi
if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi
export -p > "${package_name}.build-env"
