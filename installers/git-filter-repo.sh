#! /bin/sh -eu
anitya_project_id=63454
package_name=git-filter-repo

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"
cd "$source_dir"
  make -j -l $(nproc) prefix="$install_prefix" install
cd -
