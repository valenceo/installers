#! /bin/sh
export VERSION='5.29.0'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  mvn package
  assembly_path="${package_name}-assembly/target/${package_name}-${VERSION}-bin/"
  if ! [ -d "$assembly_path" ]; then echo "Assembly not found"; exit 10; fi
  cp -Ru -T "${assembly_path}/${package_name}-${VERSION}" "$install_prefix"
cd -

# Conflicts with dcmtk
#cd "$dotfiles_dir"
#  . hooks/post_install.sh
#cd -
