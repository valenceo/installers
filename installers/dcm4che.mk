#! /usr/bin/make -rf

%/src/${package_name} :
	git clone --depth=1 -b "${VERSION}" git@github.com:dcm4che/dcm4che.git "$@"
