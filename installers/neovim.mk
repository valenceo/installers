#! /usr/bin/make -rf

%/src/${package_name} :
	git clone -b "v${VERSION}" "https://github.com/neovim/${package_name}.git" "$@"
