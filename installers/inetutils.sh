#! /bin/sh
export VERSION='2.2'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/inetutils
cd build/inetutils
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=$HOME/etc \
    --enable-encryption \
    --enable-authentication \
    --disable-ftpd \
    --disable-inetd \
    --disable-rexecd --disable-rlogind --disable-rshd \
    --disable-syslogd \
    --disable-talkd \
    --disable-telnetd \
    --disable-uucpd \
    --disable-rcp --disable-rexec --disable-rlogin --disable-rsh \
    --disable-talk
  
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
