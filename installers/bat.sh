#! /bin/sh
. templates/rust_template.sh

install -D -s ~/.cargo/bin/${package_name} ~/bin/${package_name}
<< EOF cat > ${XDG_CONFIG_HOME}/environment.d/90-bat.conf
MANPAGER="sh -c 'col -bx | bat -l man -p'"
EOF
