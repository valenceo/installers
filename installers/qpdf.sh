#! /bin/sh
export VERSION='11.1.1'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=$HOME/etc \
    --with-ssl-dir="${OPENSSL_ROOT_DIR?}" \
    --enable-crypto-openssl
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
  make -rf "${package_name}.mk" ~/.local/etc/bash_completion.d/${package_name}
cd -
