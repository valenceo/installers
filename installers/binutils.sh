#! /bin/sh
export VERSION='2.39'
set -eu
if [ -w /opt ]; then
  install_prefix="/opt"
else
  install_prefix="/usr/local/gnu"
fi
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/binutils
cd build/binutils
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --sysconfdir=/etc \
    --disable-nls \
    --with-system-zlib
  
  make all -j -l $(nproc)
  make install
cd -
