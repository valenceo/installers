#! /bin/sh
set -eu
make -rf "${package_name}.mk" /usr/share/keyrings/microsoft-archive-keyring.gpg
install -m 0644 "${package_name}.list" /etc/apt/sources.list.d/teams.list

apt update
apt install teams
