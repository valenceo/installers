VERSION ?= 7.3.4

TMPDIR ?= $( shell xdg-user-dir DOWNLOAD )

/opt/google/earth/pro/googleearth : ${TMPDIR}/google-earth-pro-stable_${VERSION}_amd64.deb
	${SUDO} dpkg -i $^

${TMPDIR}/%.deb :
	curl -fL -o "$@" "https://dl.google.com/dl/linux/direct/$(@F)"

