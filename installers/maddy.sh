#! /bin/sh
export VERSION=0.5.2
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

build_dir=$( realpath build/maddy )
cd "$source_dir"
  ./build.sh --builddir "$build_dir"      build
  ./build.sh --prefix   "$install_prefix" install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
  $SUDO systemctl daemon-reload
cd -
