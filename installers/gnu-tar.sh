#! /bin/sh
export VERSION='1.34'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/tar-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -
type busybox || install-package busybox
mkdir -p build/tar
cd build/tar
  RSH=$(which ssh) "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --sysconfdir=$HOME/etc \
    --disable-nls \
    --with-gzip="busybox gzip" \
    --with-bzip2="busybox bzip2" \
    --with-lzma="busybox lzma" \
    --with-lzop="busybox lzop" \
    --with-xz="busybox xz"
  
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
