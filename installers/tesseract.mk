#! /usr/bin/make -rf

src/${package_name} :
	mkdir -p "$@"
	git clone --depth=1 -b ${VERSION} https://github.com/tesseract-ocr/$@.git
	cd "$@" ; sh autogen.sh

%.traineddata :
	curl -fL --create-dirs -o "$@" "https://github.com/tesseract-ocr/tessdata_best/raw/main/$(@F)"
