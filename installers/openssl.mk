#! /usr/bin/make -rf
%/src/${package_name} :
	git clone -b openssl-${VERSION} --depth=1 "git://git.openssl.org/${package_name}.git" "$@"

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/openssl-% :
	curl -fL -o "$@"        "https://www.openssl.org/source/$(@F)" \
		 -o "$@.sha256" "https://www.openssl.org/source/$(@F).sha256" \
		 -o "$@.asc"    "https://www.openssl.org/source/$(@F).asc"
	gpg --verify "$@.asc"
