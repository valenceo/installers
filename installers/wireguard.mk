#! /usr/bin/make -rf
.PRECIOUS : %/privatekey

%/publickey : %/privatekey
	install -d "$(@D)"
	wg pubkey < "$<" > "$@"

%/privatekey :
	install -d "$(@D)"
	wg genkey > "$@"
