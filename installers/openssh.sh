#! /bin/sh
project_id=2565
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
if [ -w "/opt" ]; then
  install_prefix="/opt/${package_name}-${VERSION}"
else
  install_prefix="/usr/local/${package_name}-${VERSION}"
fi
source_dir=$( realpath "src/${package_name}-$VERSION" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/openssh
cd build/openssh
  umask 0002
  $SUDO install -d /var/lib/sshd /var/empty
  getent group sshd || $SUDO groupadd -r sshd
  getent passwd sshd || $SUDO useradd -r -g sshd -c 'sshd privsep' -m -d /var/empty -s /bin/false sshd
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --sysconfdir=/etc/ssh \
    --with-privsep-user=sshd \
    --with-privsep-path=/var/lib/sshd \
    --with-pid-dir=/run \
    --with-ssl-dir="${OPENSSL_ROOT_DIR?}" \
    --with-security-key-builtin
  make all -j -l $(nproc)
  $SUDO make install
  $SUDO chmod a+rX -R "$install_prefix"
cd -

if [ -e /etc/environment.d ]; then
  tee /etc/environment.d/"40-${package_name}.conf" << EOF
PATH=${install_prefix}/bin:\$PATH
EOF
  chmod a+r /etc/environment.d/"40-${package_name}.conf"
else
  tee /etc/profile.d/"40-${package_name}.sh" << EOF
PATH=${install_prefix}/bin:\$PATH
EOF
  chmod a+r /etc/profile.d/"40-${package_name}.sh"
fi
