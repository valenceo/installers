#! /usr/bin/env make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/git-filter-repo-${VERSION} :
	git clone --depth 1 -b "v${VERSION}" "https://github.com/newren/git-filter-repo" "$@"
