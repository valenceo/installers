#! /bin/sh
export VERSION=latest
set -eu
export install_prefix="/opt/miniconda3"

make -rf "${package_name}.mk" "$install_prefix"
chown :staff -R "${install_prefix}/pkgs"
chmod g+wXs -R "${install_prefix}/pkgs"
tmp_script=$( mktemp )
if ${install_prefix}/bin/conda shell.bash hook > "$tmp_script"; then
  install -D "$tmp_script" "${XDG_CONFIG_HOME}/bash/${package_name}.bashrc"
fi
