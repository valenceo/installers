#! /bin/sh
project_id=9037
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.stable_versions | first' )

set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
build_dir=$( realpath "build/${package_name}" )
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  make \
    CMAKE_BUILD_TYPE=RelWithDebInfo \
    CMAKE_INSTALL_PREFIX="$install_prefix" \
    -j -l $(nproc) install
cd -

cd "$installer_dir"
  stow -S -t "$HOME" --no-folding "${package_name}/"
  . hooks/post_install.sh
cd -

<< EOF cat
Your VSCode Preferences -> User Settings are:
{
  "vim.neovimPath": "${install_prefix}/bin/nvim",
  "vim.enableNeovim": true
}
EOF
