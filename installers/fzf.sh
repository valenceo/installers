#! /bin/sh
export VERSION=0.35.0
set -eu
install_prefix="$HOME"
. /etc/environment.d/50-golang.conf
export PATH

cd "$installer_dir"
  install -d ${install_prefix}/.vim/pack/plugins
  make -B -rf "${package_name}.mk" \
    ${install_prefix}/bin/fzf \
    ${install_prefix}/bin/fzf-tmux \
    ${install_prefix}/.vim/pack/plugins/start/fzf/plugin \
    ${install_prefix}/.vim/pack/plugins/start/fzf.vim/plugin \
    ${XDG_CONFIG_HOME}/bash/fzf-key-bindings.bashrc \
    ${XDG_DATA_HOME}/bash-completion/completions/fzf.bashrc
cd -
