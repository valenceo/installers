#! /bin/sh
export VERSION='7.93'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --without-zenmap
  make -j -l $(nproc)
  make install
cd -
#libtool --finish $install_prefix/lib

cd "$installer_dir"
  . hooks/post_install.sh
cd -
