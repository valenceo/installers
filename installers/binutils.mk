TMPDIR ?= $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.lz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/binutils-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/binutils/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/binutils/$(@F).sig"
	-gpg --verify "$@.sig"
