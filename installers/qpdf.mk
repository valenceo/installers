#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name} :
	git clone -b release-${package_name}-${VERSION} --depth=1 "https://github.com/qpdf/${package_name}.git" "$@"

%/bash_completion.d/${package_name} :
	mkdir -p "$(@D)"
	qpdf --completion-bash > "$@"
