#! /bin/sh
export VERSION=1.20.1
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

build_dir=$( realpath build/${package_name} )
cd "$source_dir"
  # openssl here points to library sources!
  ./configure \
    --prefix="$install_prefix" \
    --conf-path="${HOME}/etc/nginx/nginx.conf" \
    --pid-path="${XDG_RUNTIME_DIR}/nginx/nginx.pid" \
    --error-log-path="${HOME}/logs/nginx-error.log" \
    --http-log-path="${HOME}/logs/nginx-access.log" \
    --with-threads \
    --with-http_ssl_module \
    --with-stream=dynamic \
    --with-mail=dynamic \
    --with-mail_ssl_module \
    --builddir="$build_dir"
  make -j -l $(nproc)
  make install
cd -
