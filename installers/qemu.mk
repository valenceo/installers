TMPDIR ?= $(shell mktemp -d)

%/src/${package_name} :
	git clone --depth=1 -b "v${VERSION}" "git@gitlab.com:qemu-project/$(@F).git" "$@"

