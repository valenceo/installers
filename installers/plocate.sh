#! /bin/sh
export VERSION='1.1.15'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

meson setup \
  --prefix="$install_prefix" \
  --localstatedir="$XDG_STATE_HOME" \
  --sharedstatedir="$XDG_STATE_HOME" \
  --sysconfdir="$HOME/etc" \
  -Dlocategroup=$( id -gn ) \
  -Dsystemunitdir="$XDG_CONFIG_HOME/systemd/user" \
  build/plocate "$source_dir"
#getent group plocate || $SUDO groupadd --system plocate
ninja -C build/plocate install
systemctl daemon-reload --user

cd "$installer_dir"
  . hooks/post_install.sh
cd -

echo "systemctl --user enable --now plocate-updatedb.service"
