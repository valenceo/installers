#! /bin/sh
export VERSION='12.2.0'
set -eu
if [ -w /opt ]; then
  install_prefix="/opt/${package_name}-${VERSION}"
else
  install_prefix="/usr/local/${package_name}-${VERSION}"
fi
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -
install-package binutils
export PATH="/opt/bin:$PATH"
mkdir -p build/gcc
cd build/gcc
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --sysconfdir=/etc \
    --with-build-time-tools="/opt/bin" \
    --with-system-zlib \
    --disable-multilib
  
  make all -j -l $(nproc)
  make install
cd -
PATH="${install_prefix}/bin:$PATH" install-package binutils
if [ -e /etc/environment.d ]; then
  tee /etc/environment.d/"15-${package_name}.conf"
else
  tee /etc/profile.d/"15-${package_name}.sh"
fi << EOF
#LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:${install_prefix}/lib
PATH=${install_prefix}/bin:\$PATH
EOF
