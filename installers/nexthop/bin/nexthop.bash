#! /usr/bin/env bash
set -e

for arg
do
  bzgrep -F " ${arg}" "${HOME}/share/oix-route-views/oix-full-snapshot-latest.dat.bz2"
done
