#! /bin/sh
export VERSION=5.2.0
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )


cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/tesseract
cd build/tesseract
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=$HOME/etc \
    --disable-legacy
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
