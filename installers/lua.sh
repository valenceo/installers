#! /bin/sh
set -eu
case $VERSION in
  5.3) VERSION=5.3.6 ;;
  5.4) VERSION=5.4.3 ;;
esac

install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

cd "$source_dir"
  install -d "$install_prefix"
  make INSTALL_TOP=$( realpath "$install_prefix" ) linux test install
cd -
