#! /bin/sh
export VERSION='3.0.12'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/gdcm
cd build/gdcm
  cmake -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DCMAKE_BUILD_TYPE=Release \
    -DGDCM_BUILD_SHARED_LIBS=ON \
    -DGDCM_WRAP_PYTHON=ON \
    -DGDCM_WRAP_CSHARP=OFF \
    -DGDCM_WRAP_JAVA=OFF \
    -DGDCM_WRAP_PHP=OFF \
    -DGDCM_USE_VTK=OFF \
    -DGDCM_BUILD_APPLICATIONS=ON \
    -DGDCM_BUILD_TESTING=OFF \
    -DGDCM_DOCUMENTATION=ON \
    -DDCM_BUILD_EXAMPLES=ON \
    $source_dir/
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
