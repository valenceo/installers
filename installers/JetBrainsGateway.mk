#! /usr/bin/make -rf

${TMPDIR}/JetBrainsGateway-% :
	curl -fL -O "$@" "https://download.jetbrains.com/idea/gateway/$(@F)"
