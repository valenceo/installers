#! /usr/bin/make -rf

TMPDIR ?= $(shell mktemp -d)


%/1password-archive-keyring.gpg %/keyrings/AC2D62742012EA22/debsig.gpg :
	install -d "$(@D)"
	curl -fL "https://downloads.1password.com/linux/keys/1password.asc" | gpg --dearmor --output "$@"

%/${package_name}.pol :
	install -d "$(@D)"
	curl -fL -o "$@" "https://downloads.1password.com/linux/debian/debsig/$(@F)"


%/bash-completion/completions/${package_name}.bashrc :
	install -d "$(@D)"
	op completion bash > "$@"

%/bash_completion.d/${package_name}.bashrc :
	install -d "$(@D)"
	op completion bash > "${TMPDIR}/$(@F)"
	bash -n "${TMPDIR}/$(@F)"
	install -D "${TMPDIR}/$(@F)" "$@"
