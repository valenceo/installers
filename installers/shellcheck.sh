#! /bin/sh -eu
cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cabal update
cabal install ShellCheck

install -s ~/.cabal/bin/${package_name} ~/bin/
