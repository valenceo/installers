#! /bin/sh
export VERSION='1.15'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  make CFG_DIR="${XDG_CONFIG_HOME}/lcov" PREFIX="$install_prefix" install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
