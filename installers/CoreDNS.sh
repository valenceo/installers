#! /bin/sh
set -x
export VERSION='1.10.0'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"

make -rf "${package_name}.mk" $( realpath build/${package_name}-${VERSION} )
cd build/${package_name}-$VERSION
  install -g staff -d "${install_prefix}/sbin/"
  install -D -g staff coredns "${install_prefix}/sbin/"
cd -

if ! getent group coredns; then
  $SUDO groupadd -r coredns
fi
if ! getent passwd coredns; then
  $SUDO useradd -d /run -g coredns -s /usr/sbin/nologin -r coredns
fi

$SUDO install -o coredns -g coredns -d /etc/coredns
# Adapted from https://github.com/coredns/deployment/blob/master/systemd/coredns.service
<< EOF $SUDO tee "/etc/systemd/system/coredns.service"
[Unit]
Description=CoreDNS DNS server
Documentation=https://coredns.io
After=network.target

[Service]
PermissionsStartOnly=true
LimitNOFILE=1048576
LimitNPROC=512
CapabilityBoundingSet=CAP_NET_BIND_SERVICE
AmbientCapabilities=CAP_NET_BIND_SERVICE
NoNewPrivileges=true
User=coredns
WorkingDirectory=~
ExecStart=${install_prefix}/sbin/coredns -conf=%E/coredns/Corefile
ExecReload=/bin/kill -SIGUSR1 \$MAINPID
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

$SUDO systemctl daemon-reload
