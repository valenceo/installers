#! /bin/sh
VERSION=222.4345.14
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

make -rf "${installer_dir}/${package_name}.mk" \
  $TMPDIR/JetBrainsGateway-${VERSION}.tar.gz

tar -C "${packaging_prefix}" -xaf "$TMPDIR/JetBrainsGateway-${VERSION}.tar.gz"
<< EOF cat > "${install_prefix}/.stow-local-ignore"
build.txt
Install-Linux-tar.txt
product-info.json
EOF
