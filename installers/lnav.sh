#! /bin/sh
project_id=1833
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '[.stable_versions[] | select(test("-test") | not)] | first' )

set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

mkdir -p build/lnav
cd build/lnav
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=/etc
  # Somehow -j -l can still lead to runaway load here
  make -j 4
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
