#! /usr/bin/make -rf

%/microsoft-archive-keyring.gpg :
	curl -fL "https://packages.microsoft.com/keys/microsoft.asc" | \
		gpg --dearmor -o "$@"
