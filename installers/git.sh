#! /bin/sh
anitya_project_id=5350
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${anitya_project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.stable_versions | first' )
export package_name=git
. hooks/setup.sh
. ${hooks_dir}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"

cd "$source_dir"
  make -B configure
  ./configure \
    --prefix="$install_prefix" \
    --localstatedir=/var \
    --runstatedir=/run \
    --sysconfdir=/etc \
    --with-editor=/usr/bin/sensible-editor \
    --with-pager=/usr/bin/sensible-pager
  make -j -l $(nproc) all
  make doc info  # Un-parallizable, apparently
  make install install-doc install-html install-info
cd -

cd "$installer_dir"
  install -d $XDG_CONFIG_HOME/git
  stow -t "$HOME" --no-folding -S "$package_name"
  make -rf "${package_name}.mk" $XDG_CONFIG_HOME/git/config
cd -

. hooks/post_install.sh

install -d "${XDG_CONFIG_HOME}/environment.d"
<< EOF cat > ${XDG_CONFIG_HOME}/environment.d/20-${package_name}.conf
GIT_CREDENTIAL_STORE="$XDG_RUNTIME_DIR/git"
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_SHOWSTASHSTATE=1
EOF
