#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/packages.microsoft.gpg :
	curl -fL "https://packages.microsoft.com/keys/microsoft.asc" | \
		gpg --dearmor > "$@"

