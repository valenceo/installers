#! /usr/bin/make -rf
arch ?= x86_64

TMPDIR ?= $(shell mktemp -d)

%/build/rust-${VERSION}-${arch}-unknown-linux-gnu : ${TMPDIR}/rust-${VERSION}-${arch}-unknown-linux-gnu.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/rust-% :
	curl -fL -o "$@"     "https://static.rust-lang.org/dist/$(@F)" \
	         -o "$@.asc" "https://static.rust-lang.org/dist/$(@F).asc"
	-gpg --verify "$@.asc"

