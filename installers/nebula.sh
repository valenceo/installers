#! /bin/sh
project_id=89343
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"

make -rf "${package_name}.mk" $( realpath build/${package_name}-${VERSION} )
cd build/${package_name}-$VERSION
  install -g staff -d "${install_prefix}/sbin/"
  install -D -g staff nebula nebula-cert "${install_prefix}/sbin/"
  $SUDO setcap cap_net_admin=+pe "${install_prefix}/sbin/nebula"
cd -

if ! getent group nebula; then
  $SUDO groupadd -r nebula
fi
if ! getent passwd nebula; then
  $SUDO useradd -d /run -g nebula -s /usr/sbin/nologin -r nebula
fi

cd "$installer_dir"
  stow -t "$HOME" --no-folding -S "$package_name"
cd -
install -d "${XDG_CONFIG_HOME}/systemd/user"
<< EOF cat > "${XDG_CONFIG_HOME}/systemd/user/nebula@.service"
[Unit]
Description=Nebula Service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=5
WorkingDirectory=%E/${package_name}
ExecStart=${install_prefix}/sbin/nebula -config %i.yml

[Install]
WantedBy=default.target
EOF
systemctl --user daemon-reload
