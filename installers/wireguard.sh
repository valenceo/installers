#! /bin/sh
set -eu

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "${HOME}/etc/wireguard/publickey"
fi

cd "$installer_dir"
  stow -S -t "$HOME" --no-folding "$package_name"
cd -
