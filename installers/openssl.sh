#! /bin/sh
project_id=2566
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
if [ -w "/opt" ]; then
  install_prefix="/opt/${package_name}-${VERSION}"
  $SUDO tee /etc/ld.so.conf.d/opt-openssl.conf << EOF
${install_prefix}/lib64
EOF
else
  install_prefix="/usr/local/${package_name}-${VERSION}"
fi
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/openssl
cd build/openssl
  umask 0002
  "$source_dir/config" \
    --prefix="$install_prefix" \
    --openssldir="/usr/local/ssl" \
    enable-fips \
    shared \
    zlib-dynamic
  make -j -l $(nproc)
  $SUDO install -m 0755 -d /usr/local/ssl
  $SUDO make install
  $SUDO chmod a+rX -R "$install_prefix"
cd -

# Unsure where MANPATH is altered
if [ -e /etc/environment.d ]; then
  tee /etc/environment.d/"10-${package_name}.conf" << EOF
PATH=${install_prefix}/bin:\$PATH
EOF
  chmod a+r /etc/environment.d/"10-${package_name}.conf"
else
  tee /etc/profile.d/"10-${package_name}.sh" << EOF
PATH=${install_prefix}/bin:\$PATH
EOF
  chmod a+r /etc/profile.d/"10-${package_name}.sh"
fi

$SUDO ldconfig
