#! /usr/bin/env make -rf
VERSION ?= 1.22.2
TMPDIR ?= $(shell mktemp -d)

install : ${TMPDIR}/deltachat-desktop_${VERSION}_amd64.deb
	${SUDO} apt install "$<"

${TMPDIR}/deltachat-%.deb ${TMPDIR}/DeltaChat% :
	curl -fL -o "$@" "https://download.delta.chat/desktop/v${VERSION}/$(@F)"

