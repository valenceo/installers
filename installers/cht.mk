%/bin/cht.sh :
	install -d "$(@D)"
	curl -fL -o "$@" "https://cht.sh/:cht.sh"
	-chmod +x "$@"

%/bash_completion.d/cht.sh : ${HOME}/bin/cht.sh
	install -d "$(@D)"
	curl -fL -o "$@" "https://cheat.sh/:bash_completion"
