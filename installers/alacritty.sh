#! /bin/sh
export VERSION='0.11.0'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )
. /etc/environment.d/50-rust.conf
export PATH

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cargo install --path "$source_dir/$package_name"
gzip "$source_dir/extra"/*.man || :

cd "$installer_dir"
  install -d "${install_prefix}/bin/" "${install_prefix}/share/man/man1"
  install -D -s ~/.cargo/bin/alacritty "$install_prefix/bin/"
  install -m 0644 -C "${source_dir}/extra/logo/alacritty-term.svg" "${install_prefix}/bin/"
  install -D "${source_dir}/extra"/*.man.gz "${install_prefix}/share/man/man1"
  install -C -D "${source_dir}/extra/completions/alacritty.bash" \
    "${install_prefix}/etc/bash_completion.d/${package_name}.bashrc"
  if ! infocmp alacritty > /dev/null; then
    $SUDO tic -xe alacritty,alacritty-direct "${source_dir}/extra/alacritty.info"
  fi
  $SUDO update-alternatives --install \
    $(which x-terminal-emulator) x-terminal-emulator \
    "${install_prefix}/bin/${package_name}" 39
  stow -S -t "$HOME" --no-folding "$package_name"
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -

