#! /usr/bin/env make -rf

${HOME}/bin/${package_name} : ${HOME}/go/bin/${package_name}
	install -D -s "$<" "$@"
	install -D -s "$(<D)/age-keygen" "$(@D)/age-keygen"

${HOME}/bin/age-plugin-yubikey :
	cargo install "$(@F)"
	install -D -s "${HOME}/.cargo/bin/$(@F)" "$@"

%/go/bin/age :
	go install filippo.io/$(@F)/cmd/...@latest

${HOME}/.pki/age/%.key :
	install -d "$(@D)"
	# might contain multiple keys
	age-keygen -o "$@"
