#! /usr/bin/env make -rf
TMPDIR ?= $(shell mktemp -d)
YEAR ?= $(shell date +%Y)

%/src/sqlite-autoconf-${VERSION} : ${TMPDIR}/sqlite-autoconf-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/sqlite-% :
	curl -fl -o "$@" "https://www.sqlite.org/${YEAR}/$(@F)"
