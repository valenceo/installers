#! /bin/sh
set -eu
project_id=13254
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
case $VERSION in
  2|2.7) VERSION=$( curl -fL "$check_url" | jq -r '[.stable_versions[] | select(test("^2.7"))] | first' ) ;;
  3.7) VERSION=$( curl -fL "$check_url" | jq -r '[.stable_versions[] | select(test("^3.7"))] | first' ) ;;
  3|3.10) VERSION=$( curl -fL "$check_url" | jq -r '[.stable_versions[] | select(test("^3.10"))] | first' ) ;;
  3.11) VERSION=$( curl -fL "$check_url" | jq -r '[.stable_versions[] | select(test("^3.11"))] | first' ) ;;
esac
export VERSION

install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/Python-${VERSION}" )

codename=$( lsb_release -sc )

case ${VERSION} in
  2.*)
    if dependencies=$( sed 's/[#].*//' "python-2.deps" ) && [ -n "$dependencies" ]; then
      $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
    fi
    ;;
  3.*)
    if dependencies=$( sed 's/[#].*//' "python-3-${codename}.deps" ) && [ -n "$dependencies" ]; then
      $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
    fi
    ;;
esac
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
case $VERSION in
  2.*)
    if [ -s "python-2.stow-local-ignore" ]; then
      install -C -D python-2.stow-local-ignore ${install_prefix}/.stow-local-ignore
    fi
    ;;
  3.*)
    if [ -s "python-3.stow-local-ignore" ]; then
      install -C -D python-3.stow-local-ignore ${install_prefix}/.stow-local-ignore
    fi
    ;;
esac

case "$install_prefix" in
  */home/*)
    localstatedir=$XDG_STATE_HOME
    runstatedir=$XDG_RUNTIME_DIR
    sysconfdir="${HOME}/etc"
    ;;
  *)
    localstatedir=/var
    runstatedir=${localstatedir}/run
    sysconfdir=/etc
    ;;
esac

mkdir -p build/python-$VERSION
cd build/python-$VERSION
  # For example, --with-dtrace, --with-valgrind, --enable-profiling, --enable-pystats
  $source_dir/configure \
    --prefix="$install_prefix" \
    --localstatedir="$localstatedir" \
    --runstatedir="$runstatedir" \
    --sysconfdir="$sysconfdir" \
    --enable-shared \
    --enable-loadable-sqlite-extensions=yes \
    --with-system-expat \
    --with-system-ffi \
    --with-system-libmpdec \
    --with-openssl="${OPENSSL_ROOT_DIR?}"
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -

echo "You may need to ${install_prefix}/bin/python -m ensurepip"
