#! /usr/bin/make -rf
VERSION ?= 22.11.0.19

DOWNLOAD_DIR := $(shell xdg-user-dir DOWNLOAD)

install : ${DOWNLOAD_DIR}/icaclient_${VERSION}_amd64.deb
	${SUDO} apt install "$+"
	${SUDO} systemctl disable --now ctxlogd

${DOWNLOAD_DIR}/icaclient_% :
	@echo "Download $(@F) from 'https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html'"
	sha256sum "$@"
