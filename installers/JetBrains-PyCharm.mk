#! /usr/bin/make -rf
DOWNLOAD_DIR ?= ${shell xdg-user-dir DOWNLOAD}

%.tar.gz :
	@echo "Download '$@' from 'https://www.jetbrains.com/pycharm/download/'"

%/pycharm-community-${VERSION} : ${DOWNLOAD_DIR}/pycharm-community-${VERSION}.tar.gz
	cd "$(<D)" && sha256sum -c "$<.sha256"
	tar -C "$(@D)" -xaf "$<"

%/pycharm-${VERSION} : ${DOWNLOAD_DIR}/pycharm-professional-${VERSION}.tar.gz
	cd "$(<D)" && sha256sum -c "$<.sha256"
	tar -C "$(@D)" -xaf "$<"
