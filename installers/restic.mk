#! /usr/bin/make -rf
arch ?= linux_amd64
#arch = linux_386
VERSION ?= 0.14.0

install_prefix ?= ${packaging_prefix}/${package_name}-${VERSION}
TMPDIR := $(shell mktemp -d)

install : ${install_prefix}/bin/${package_name} ${XDG_DATA_HOME}/bash-completion/completions/${package_name}.bashrc ${install_prefix}/share/man/man1/${package_name}.1

%/src/${package_name} :
	git clone --depth=1 "https://github.com/restic/$(@F)" "$@"

${install_prefix}/bin/${package_name} : ${TMPDIR}/${package_name}_${VERSION}_${arch}.bz2
	install -d "$(@D)"
	bzip2 -d < "$<" > "$@"
	chmod +x "$@"

${TMPDIR}/${package_name}_% :
	curl -fL -o "$@" "https://github.com/restic/${package_name}/releases/download/v${VERSION}/$(@F)"

${XDG_DATA_HOME}/bash-completion/completions/${package_name}.bashrc :
	install -d "$(@D)"
	${install_prefix}/bin/restic generate --bash-completion "$@"

# Man files, section 1:
%.1 :
	install -d "$(@D)"
	${install_prefix}/bin/restic generate --man "$(@D)"

.PHONY : install
