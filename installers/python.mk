#! /usr/bin/make -rf
TMPDIR ?= $(shell mktemp -d)

%/src/Python-${VERSION} : ${TMPDIR}/Python-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/Python-% :
	curl -fL -o "$@" "https://www.python.org/ftp/python/${VERSION}/$(@F)"


${HOME}/.local/bin/pip : ${TMPDIR}/pip.pyz
	python3 "$<" install --user --upgrade pip

${TMPDIR}/pip.pyz :
	wget -O "$@" "https://bootstrap.pypa.io/pip/$(@F)"
