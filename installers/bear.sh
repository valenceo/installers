#! /bin/sh
set -eu
install_prefix="${packaging_prefix}/${package_name}"
source_dir=$( realpath "src/${package_name}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

mkdir -p build/Bear
cd build/Bear
  cmake -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DENABLE_UNIT_TESTS=OFF -DENABLE_FUNC_TESTS=OFF \
    "$source_dir"
  make -j -l $(nproc)
  make install
cd -
