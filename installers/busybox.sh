#! /bin/sh
export VERSION='1.35.0'
set -eu
install_prefix=/usr/local
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/busybox
cd build/busybox
  if ! cp -v "${installer_dir}/${package_name}.config" .config; then
    make KBUILD_SRC="${source_dir}" -f "${source_dir}/Makefile" menuconfig
  fi
  install -d "${install_prefix}/bin"
  make -j -l $(nproc)
  if command -v upx; then
    upx -o "${install_prefix}/bin/busybox" busybox
  else
    install -s busybox "${install_prefix}/bin/busybox"
  fi
cd -
