#! /usr/bin/make -rf
.PHONY: terminfo
TMPDIR ?= $(shell mktemp -d)

%/src/${package_name} :
	git clone -b v${VERSION} --depth=1 https://github.com:alacritty/${package_name}.git "$@"


terminfo : ${TMPDIR}/${package_name}.info
	tic -xe ${package_name},${package_name}.direct "$<"


%/${package_name}.info :
	mkdir -p "$(@D)"
	curl -fL -o "$@" \
		"https://raw.githubusercontent.com/alacritty/${package_name}/master/extra/$(@F)"

