#! /bin/sh
set -eu

cd "$installer_dir"
  if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
    $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
  fi
cd -

make -rf "${package_name}.mk" /etc/apt/trusted.gpg.d/packages.microsoft.gpg
install -m 0644 "${package_name}.list" /etc/apt/sources.list.d/vscode.list

apt update
apt install code
